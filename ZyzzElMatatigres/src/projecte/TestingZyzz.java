package projecte;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestingZyzz {
	private int[][] mat;

    @BeforeEach
    public void setUp() {
        mat = new int[10][10];
    }

    @Test
    public void cas1() {
    	//Cas1
    	 String result = ZyzzElMatatigres.principal(mat, 3, 5, 5, 5);
    	 //System.out.println(result);
         assertEquals("Zyzz aquí mira",result); // Ajusta según tus expectativas
    }
    
    @Test
    public void cas2() {
       
         //cas2
         String result2 = ZyzzElMatatigres.principal(mat, 9, 9, 0, 0);
    	 //System.out.println(result2);
    	 assertEquals("Le mogut 6 posicions al tigre",result2);
    	
    }
    @Test
    public void cas3() {
    	 //cas3
    	 String result3 = ZyzzElMatatigres.principal(mat, 0, 0, 1, 0);
    	 //System.out.println(result3);
    	 assertEquals("Molt bé Zyzz",result3);
    }
    @Test
    public void cas4() {
      
    	 //cas4
    	 String result4 = ZyzzElMatatigres.principal(mat, 7, 5, 9, 2);
    	 //System.out.println(result4);
    	 assertEquals("Zyzz aquí mira",result4);
    
    }
    @Test
    public void cas5() {
      
    	 //cas5
    	 String result5 = ZyzzElMatatigres.principal(mat, 2, 3, 7, 8);
    	 //System.out.println(result5);
    	 assertEquals("Le mogut 2 posicions al tigre",result5);
    	
    }
    @Test
    public void cas6() {
       
    	 //cas6
    	 String result6 = ZyzzElMatatigres.principal(mat, 6, 7, 6, 7);
    	 //System.out.println(result6);
    	 assertEquals("Molt bé Zyzz",result6);
    }
    @Test
    public void cas7() {
    	 String result7 = ZyzzElMatatigres.principal(mat, 1, 1, 0, 5);
    	 //System.out.println(result7);
    	 assertEquals("Le mogut 1 posicions al tigre",result7);
    }
    @Test
    public void cas8() {
    	String result8 = ZyzzElMatatigres.principal(mat, 3, 5, 2, 4);
   	 	//System.out.println(result7);
   	 	assertEquals("Molt bé Zyzz",result8);
    }
    @Test
    public void cas9() {
    	 String result9 = ZyzzElMatatigres.principal(mat, 3, 5, 9, 8);
    	 //System.out.println(result9);
    	 assertEquals("Le mogut 3 posicions al tigre",result9);
    }
    @Test
    public void cas10() {
    	 String result10 = ZyzzElMatatigres.principal(mat, 5, 5, 6, 5);
    	 //System.out.println(result10);
    	 assertEquals("Molt bé Zyzz",result10);
    }
    @Test
    public void cas11() {
    	 String result11 = ZyzzElMatatigres.principal(mat, 6, 0, 4, 2);
    	 //System.out.println(result11);
    	 assertEquals("Zyzz aquí mira",result11);
    }
    @Test
    public void cas12() {
    	 String result12 = ZyzzElMatatigres.principal(mat, 2, 1, 3, 3);
    	 //System.out.println(result12);
    	 assertEquals("Zyzz aquí mira",result12);
    }
    @Test
    public void cas13() {
    	 String result13 = ZyzzElMatatigres.principal(mat, 7, 4, 0, 1);
    	 //System.out.println(result13);
    	 assertEquals("Le mogut 4 posicions al tigre",result13);
    }
    @Test
    public void cas14() {
    	 String result14 = ZyzzElMatatigres.principal(mat, 8, 3, 0, 5);
    	 //System.out.println(result14);
    	 assertEquals("Le mogut 5 posicions al tigre",result14);
    }
    @Test
    public void cas15() {
    	 String result15 = ZyzzElMatatigres.principal(mat, 8, 1, 8, 2);
    	 //System.out.println(result15);
    	 assertEquals("Molt bé Zyzz",result15);
    }
    @Test
    public void cas16() {
    	 String result16 = ZyzzElMatatigres.principal(mat, 0, 0, 0, 0);
    	 //System.out.println(result16);
    	 assertEquals("Molt bé Zyzz",result16);
    }
    @Test
    public void cas17() {
    	 String result17 = ZyzzElMatatigres.principal(mat, 6, 9, 9, 6);
    	 //System.out.println(result17);
    	 assertEquals("Zyzz aquí mira",result17);
    }
    @Test
    public void cas18() {
    	 String result18 = ZyzzElMatatigres.principal(mat, 4, 4, 9, 9);
    	 //System.out.println(result18);
    	 assertEquals("Le mogut 2 posicions al tigre",result18);
    }
    @Test
    public void cas19() {
    	 String result19 = ZyzzElMatatigres.principal(mat, 6, 3, 3, 6);
    	 //System.out.println(result19);
    	 assertEquals("Zyzz aquí mira",result19);
    }
    @Test
    public void cas20() {
    	 String result20 = ZyzzElMatatigres.principal(mat, 2, 2, 2, 3);
    	 //System.out.println(result7);
    	 assertEquals("Molt bé Zyzz",result20);
    }
}
