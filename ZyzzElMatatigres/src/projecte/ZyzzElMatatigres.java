package projecte;

import java.util.Iterator;
import java.util.Scanner;

public class ZyzzElMatatigres {

	static Scanner sc=new Scanner(System.in);
	public static void main(String[] args) {
		// Zyzz El Matatigres
		int [][] mat= new int[10][10];
		
		int fGat=sc.nextInt();
		int cGat=sc.nextInt();
		
		int fMosc=sc.nextInt();
		int cMosc=sc.nextInt();
		
		String respuesta=principal(mat,fGat,cGat,fMosc,cMosc);
		System.out.println(respuesta);

	}
	
	public static String principal(int[][] mat, int fGat,int cGat, int fMosc, int cMosc) {
		
		String resultat="";
		modificarMatriz(mat,fGat,cGat,fMosc,cMosc);
		//printarMatriz() Por si quieres ver donde esta la mosca y el gato
		if(respuesta1(mat,fGat,cGat,fMosc,cMosc)) {
			resultat="Molt bé Zyzz";
		}else if(respuesta2(mat,fGat,cGat,fMosc,cMosc)){
			resultat="Zyzz aquí mira";
		}else {
			int contF=3;
			int contC=3;
			int contador=0;
			resultat=respuesta3(mat,fGat,cGat,fMosc,cMosc,contF,contC,contador);
		}
		
		return resultat;
		
	}
	
	public static void printarMatriz(int mat[][]) {
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[0].length; j++) {
				
				System.out.print(mat[i][j]+" ");
			}
			System.out.println("");
		}
		System.out.println();
	}
	
	public static void modificarMatriz(int mat[][], int fGat, int cGat,int fMosc, int cMosc) {
		
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[0].length; j++) {
				 if(i==fMosc && j== cMosc) {
					mat[i][j]=2;
				}
				 if(i==fGat && j== cGat) {
						mat[i][j]=1;
					}
				 
			}
			System.out.println("");
		}
		
		
	}
	
	
	public static boolean respuesta1(int[][] mat, int fGat, int cGat, int fMosc, int cMosc) {
		
		boolean flat=false;
		for (int i = fGat-1; i <= fGat+1; i++) {
			for (int j = cGat-1; j <= cGat+1; j++) {
				if(!meSalgo(mat,i,j) && i==fMosc && j==cMosc) {
					flat=true;
				}
			}
		}
		
		return flat;
	}
	
	public static boolean respuesta2(int[][] mat, int fGat, int cGat, int fMosc, int cMosc) {
		
		boolean flat=false;
		for (int i = fGat-3; i <= fGat+3; i++) {
			for (int j = cGat-3; j <= cGat+3; j++) {
				if(!meSalgo(mat,i,j) && i==fMosc && j==cMosc) {
					flat=true;
				}
			}
		}
		
		return flat;
	}
	
	
	public static String respuesta3(int[][] mat, int fGat, int cGat,
			int fMosc, int cMosc,int contF,int contC, int contador) {
		
		String respuesta="";
		boolean flat=false;
		while(!flat) {
			contador++;
			contF++;
			contC++;
			for (int i = fGat-contF; i <= fGat+contC; i++) {
				for (int j = cGat-contF; j <= cGat+contC; j++) {
					if(!meSalgo(mat,i,j) && mat[i][j]==2) {
						String change=Integer.toString(contador);
						respuesta="Le mogut "+change+" posicions al tigre";
						flat=true;
						break;
					}
					
				}
			}
		
		}
		return respuesta;
	}
	
	private static boolean meSalgo(int[][]mat, int f, int c) {
		if(f<0 || c<0 || f>mat.length-1 || c>mat[0].length-1 ) {
			return true;
		}else {
			return false;
		}
	}

}
